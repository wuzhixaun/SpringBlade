package org.springblade.system;


import org.springblade.core.launch.constant.AppConstant;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * 系统模块启动器
 * @author Chill
 */
@SpringBootApplication
@EnableFeignClients(AppConstant.BASE_PACKAGES)
public class SystemApplication {

	public static void main(String[] args) {
		SpringApplication.run(SystemApplication.class, args);
	}

}

